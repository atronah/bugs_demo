[Chapter 1](subfolder/target_file.md#chapter-1) - that link doesn't work until you add link to whole file target_file.md into this file.

To solve that problem you can either 
add link to whole target file like `[target file](subfolder/target_file.md)`
or add link with relative path like `[Chapter 1](./target_file.md#chapter-1)`.