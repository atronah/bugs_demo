# bugs_demo


## Bug with Markdown links to header id anchors

- [target file](subfolder/target_file.md) - links from README work fine
    - [Chapter 1](subfolder/target_file.md#chapter-1) - with abcolutely path
    - [Chapter 2](./subfolder/target_file.md#chapter-2) - with relative path
- [source file](subfolder/source_file.md) - links from that file to chapters in [target file](subfolder/target_file.md) don't work in some cases
